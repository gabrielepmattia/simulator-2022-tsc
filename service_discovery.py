import random
from typing import List

"""
Implementation of the discovery service for nodes
"""


class ServiceDiscovery:
    def __init__(self, nodes):
        self._nodes = {}  # id: Node
        self._neighbors = {}  # node_id: [1,2,3]

        for node in nodes:
            self.add_node(node)

    def add_node(self, node):
        self._nodes[node.get_id()] = node

    def get_nodes(self):
        return self._nodes

    def set_neighbors_for_node(self, node_id, neighbors):
        """Set neighbors for passed node"""
        self._neighbors[node_id] = neighbors

    def get_neighbors_for_node(self, node_id):
        neighbors = []
        for node_id in self._neighbors[node_id]:
            neighbors.append(self._nodes[node_id])
        return neighbors

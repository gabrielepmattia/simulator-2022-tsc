import math
import sys

import simpy

from log import Log
from models import NodeParams
from node import Node
from service_datastorage import ServiceDataStorage
from service_discovery import ServiceDiscovery

"""
Run the simulation 
"""

MODULE = "Simulate"

SIMULATION_TIME = 50000
SIMULATION_TOTAL_TIME = SIMULATION_TIME + 100
NODES = 2  # number of nodes
ROUND_PERIOD = 300

L_RATE = 6


def simulate(env):
    Log.minfo(MODULE, "Initializing nodes and parameters")
    Log.mdebug(MODULE, "total_time=%d, n_nodes=%d, l=%.3f" % (SIMULATION_TOTAL_TIME, NODES, L_RATE))

    nodes = []
    params = NodeParams()
    params.l_rate = L_RATE
    params.simulation_time = SIMULATION_TIME
    params.n_servers = 1
    params.mi_rate = 10  # ro = 0.6
    nodes.append(Node(node_id=0, env=env, params=params, round_period=ROUND_PERIOD))

    params = NodeParams()
    params.l_rate = L_RATE
    params.simulation_time = SIMULATION_TIME
    params.n_servers = 1
    params.mi_rate = 6.5  # ro = 0.92
    nodes.append(Node(node_id=1, env=env, params=params, round_period=ROUND_PERIOD))

    # add them discovery service
    discovery = ServiceDiscovery(nodes)
    data_storage = ServiceDataStorage(nodes)

    discovery.set_neighbors_for_node(0, [1])
    discovery.set_neighbors_for_node(1, [0])

    # add services in them
    for node in nodes:
        node.set_discovery_service(discovery)
        node.set_datastorage_service(data_storage)

    # info process
    def process_info(env):
        while True:
            yield env.timeout(1)
            total_blocks = 30
            progress = (env.now / SIMULATION_TOTAL_TIME) * 100
            progress_blocks = int(math.ceil(progress * total_blocks / 100))
            print("\r[Simulate] Progress [" + "=" * progress_blocks + " " * (total_blocks - progress_blocks) + "] %.2f%% [t=%d]" % (
                progress, env.now), end="")

    def process_info_round(env):
        round_number = 0
        nodes = discovery.get_nodes()
        while True:
            yield env.timeout(1)
            if env.now % (ROUND_PERIOD -1) == 0:
                print(f"[Simulate] Round#{round_number} [t={env.now}] | ", end="")
                for node_id in nodes.keys():
                    print(f"{node_id}={round(nodes[node_id].get_last_latency(), 4)}", end=" ")
                print()

                data_storage.record_round_end()
                round_number += 1

    # env.process(process_info(env))
    env.process(process_info_round(env))

    Log.minfo(MODULE, "Starting simulation")
    env.run(until=SIMULATION_TOTAL_TIME)

    data_storage.print_data()
    data_storage.plot_round_delays("2_nodes")
    data_storage.plot_round_delays_difference("2_nodes")
    data_storage.plot_round_ros("2_nodes")
    data_storage.plot_round_migration_ratios("2_nodes")


def main(argv):
    env = simpy.Environment()
    simulate(env)


if __name__ == "__main__":
    main(sys.argv)

import random

from job import Job

"""
Module for utility classes
"""


class NodeParams:
    """Configurable parameters for a node"""

    def __init__(self):
        self.l_rate = 0.95
        """Job arrival rate"""
        self.k = 1
        """Time for getting the node load state"""
        self.simulation_time = 40000
        """Total time of the simulation"""
        # self.theta = 5
        """Threshold from which starting to cooperate"""
        self.n_servers = 1
        """Number of internal servers"""
        self.mi_rate = 1
        """Service rate for jobs"""


class ProbeRequest:
    """Represents a probe request"""

    def __init__(self, to_node_id: None or int = None, load: None or int = None, job: Job or None = None):
        self._to_node_id = to_node_id
        self._load = load
        self._job = job

    def get_to_node_id(self):
        return self._to_node_id

    def get_load(self):
        return self._load

    def get_job(self):
        return self._job


class ProbeRequestNew:
    """Represents a probe request"""

    def __init__(self, to_node_id: None or int = None, load: None or int = None, job: Job or None = None,
                 old_load: None or int = None):
        self._to_node_id = to_node_id
        self._load = load
        self._job = job
        self._old_load = old_load

    def get_to_node_id(self):
        return self._to_node_id

    def get_load(self):
        return self._load

    def get_job(self):
        return self._job

    def get_old_load(self):
        return self._old_load


class StateSnapshot:
    def __init__(self, time: float, value: float):
        self._time = time
        self._value = value

    def get_time(self):
        return self._time

    def get_value(self):
        return self._value

import simpy

"""
This is the representation of a job
"""


class Job:
    def __init__(self, env: simpy.Environment or None = None, duration: float = 1.0):
        self._env = env
        """Simpy environment"""
        self._job_duration = duration  # s
        # checkpoints
        self._time_job_generated = self._env.now
        """Instant when job has been generated"""
        self._time_job_added_to_queue = self._env.now
        """Instant when job has been added to the queue"""
        self._time_job_done = self._env.now
        """Instant when job has been done, rejected or not"""
        # conditions
        self._forwarded = False
        """If the job is comes from another node"""
        self._rejected = False
        """If the job has been rejected"""
        self._executed = False
        """If the job has been executed"""
        self._done = False
        """If job path has ended"""

    #
    # Actions
    #

    def a_queued(self):
        """Called when job has beed added to the execution queue of the node"""
        self._time_job_added_to_queue = self._env.now

    def a_executed(self):
        """Called when the job has been executed"""
        self._executed = True
        self._done = True
        self._time_job_done = self._env.now

        # if self._end_clb is not None:
        #     self._end_clb(self)

    def a_forwarded(self, to_node_id, latency, net_speed):
        """Called when job has been forwarded"""
        self._forwarded = True

    def a_rejected(self):
        """Called when job has been rejected and it will be never executed"""
        self._rejected = True
        self._done = True
        self._time_job_done = self._env.now

        # if self._end_clb is not None:
        #     self._end_clb(self)

    #
    # Exported
    #

    def get_job_duration(self) -> float:
        return self._job_duration

    def get_total_execution_time(self) -> float:
        return self._time_job_done - self._time_job_added_to_queue

    def is_rejected(self):
        return self._rejected

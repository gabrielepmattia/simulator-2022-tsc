import random

MIGRATION_RATIOS = [0.3, 0.2]
MIGRATION_DECISIONS = [0 for _ in range(len(MIGRATION_RATIOS) + 1)]


def is_job_to_forward():
    random_pick = random.random()
    cumulative_ratio = 0.0
    for i, ratio in enumerate(MIGRATION_RATIOS):
        cumulative_ratio += ratio
        if random_pick < cumulative_ratio and cumulative_ratio > 0.0:
            return i
    return -1


for i in range(1000):
    decision = is_job_to_forward()
    MIGRATION_DECISIONS[decision + 1] += 1

print(MIGRATION_DECISIONS)
print([round(n / sum(MIGRATION_DECISIONS), 2) for n in MIGRATION_DECISIONS])
print(sum(MIGRATION_DECISIONS))
